package ictgradschool.industry.lab07.ex06;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab07.ex05.InvalidIndexException;

/**
 * TODO Write a program according to the Exercise Six guidelines in your lab handout.
 */
public class ExerciseSix {

    public void start() {
        // TODO Write the codes :)

        // Initialise the string to store input
        String userInput = "";

        // Get user input for string
        while (true) {
            try {
                userInput = getUserInput();
                System.out.println(userInput);
                break;
            } catch (ExceedMaxStringLengthException e1) {
                System.out.println("Your input exceeded 100 characters. Please try again.");
            } catch (InvalidIndexException e2) {
                System.out.println("Your input contain non-words. Please try again.");
            }
        }
    }

    // TODO Write some methods to help you.
    private String getUserInput() throws ExceedMaxStringLengthException, InvalidIndexException {

        // initialise string to accept user input
        String input;

        // prompt user to enter a string and set input to value of user input
        System.out.println("Enter a string of at most 100 characters. Remember that words must not start with an integer: ");
        input = Keyboard.readInput().trim();

        // check if input exceed 100 char, throw error if true
        if (input.length() > 100) {
            throw new ExceedMaxStringLengthException();
        }

        // split string into string array
        String[] inputSplit = input.trim().split(" ");

        // loop through array to check the char at first index of each array at [i]
        // if charAt(0) is a number - > throw exception
        // if charAt(0) is a char - > return first character of string array at [i]

        // Initialise a string to store all the values being returned from the exception checker
        // Append the charAt(0) for each element in the String array to this string
        // Return this as the final output
        String inputResult ="";

        for (int i = 0; i < inputSplit.length; i++) {
            if (!inputSplit[i].isEmpty()) {
                if ("1234567890".contains(Character.toString(inputSplit[i].charAt(0)))) {
                    throw new InvalidIndexException();
                } else {
                    inputResult += Character.toString(inputSplit[i].charAt(0)) + " ";
                }
            }
        }
        return inputResult.trim();
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseSix().start();
    }
}
