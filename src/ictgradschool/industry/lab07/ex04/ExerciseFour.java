package ictgradschool.industry.lab07.ex04;

import ictgradschool.Keyboard;

import java.util.Arrays;

public class ExerciseFour
{
    /**
     * Main program entry point. Do not edit, except for uncommenting the marked lines when required.
     */
	public static void main(String[] args)
	{
		ExerciseFour program = new ExerciseFour();
		
		// Exercise four, Question 1
		program.divideNumbers();

        // Exercise four, Question 2
        // TODO You may uncomment this line to help test your solution to question two.
		 program.question2();

        // Exercise four, Question 3
        // TODO You may uncomment this line to help test your solution to question three.
		 program.question3();
	}

	/**
	 * The following tries to divide using two user input numbers, but is prone to error.
     *
     * TODO Add some error handling to the code as follows:
     * TODO 1) If the user enters 0 for the second number, "Divide by 0!" should be printed instead of crashing the program.
     * TODO 2) If the user enters numbers which aren't integers, "Input error!" should be printed instead of crashing the program.
     * TODO Both these exceptional cases should be handled in the same try-catch block.
	 */
	public void divideNumbers() {
		int num1 = 0;
		int num2 = 0;
		int total = 0;

		while (true) {

			try {
				System.out.print("Enter the first number: ");
				num1 = Integer.parseInt(Keyboard.readInput());
				System.out.print("Enter the second number: ");
				num2 = Integer.parseInt(Keyboard.readInput());
				total = num1 / num2;

				// Output the result
				System.out.println("Result: " + num1 + " / " + num2 + " = " + total);
				break;

			} catch (NumberFormatException e) {
				System.out.println("Wrong format, please enter a number! ");
			} catch (ArithmeticException e) {
				System.out.println("You cannot divide by zero! ");
			}
		}
		System.out.println("------------------------------------------"); // LINE
	}

	public void question2() {

//		throw new StringIndexOutOfBoundsException("bob");
//		System.out.println("Hello World".charAt(10000));

//		//TODO Write some Java code which throws a StringIndexOutOfBoundsException
//		String str = "";
//		int index = 0;
//
//		while (true) {
//
//			try {
//				System.out.print("Please enter a sentence: ");
//				str = Keyboard.readInput();
//				System.out.print("Please enter an index: ");
//				index = Integer.parseInt(Keyboard.readInput());
//
//				// Output the result
//				System.out.println("The character at index " + index + " is " + str.charAt(index));
//				break;
//
//			} catch (NumberFormatException e) {
//				System.out.println("Wrong format, please enter a number! ");
//			} catch (StringIndexOutOfBoundsException e) {
//				System.out.println("Invalid index, please enter a number within range between 0 and " + (str.length() - 1) + "! ");
//			}
//		}
//		System.out.println("------------------------------------------"); // LINE
	}

	public void question3() {
		//TODO Write some Java code which throws a ArrayIndexOutOfBoundsException

		int[] newArray ={1,2,3,4,5,6,7,8,9,10};

		int index; // initialise int to store user specified index

		while (true) {

			try {
				System.out.print("Please enter an index: ");
				index = Integer.parseInt(Keyboard.readInput());

				// Output the result
				System.out.println("The number at index " + index + " in the array is " + newArray[index]);
				break;

			} catch (NumberFormatException e) {
				System.out.println("Wrong format, please enter a number! ");
			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("Invalid index, please enter a number within range between 0 and " + (newArray.length - 1) + "!");
			}
		}
		System.out.println("------------------------------------------"); // LINE
	}


}