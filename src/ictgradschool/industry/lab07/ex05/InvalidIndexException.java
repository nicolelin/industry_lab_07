package ictgradschool.industry.lab07.ex05;

/**
 * Created by ylin183 on 2/04/2017.
 */
public class InvalidIndexException extends Exception {
    public InvalidIndexException () {
    }

    public InvalidIndexException (String message) {
        super ("Invalid index. Please try again.");
    }

//    public InvalidIndexException (Throwable cause) {
//        super (cause);
//    }

}

//Format of custom exception:
//
//public class MyOwnException extends Exception {
//    // Parameterless Constructor
//    public MyOwnException () {
//
//    }
//    // Constructor that accepts a message
//    public MyOwnException (String message) {
//        super (message);
//    }
//
//    public MyOwnException (Throwable cause) {
//        super (cause);
//    }
//
//    public MyOwnException (String message, Throwable cause) {
//        super (message, cause);
//    }
//}